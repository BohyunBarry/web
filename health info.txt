Physical health[change | change source]
Physical fitness refers to good body health, and is the result of regular exercise, proper diet and nutrition, and proper rest for physical recovery. A person who is physically fit will be able to walk or run without getting breathless and they will be able to carry out the activities of everyday living and not need help. How much each person can do will depend on their age and whether they are a man or woman. A physically fit person usually has a normal weight for their height. The relation between their height and weight is called their Body Mass Index. A taller person can be heavier and still be fit. If a person is too heavy or too thin for their height it may affect their health. [3]

Mental health[change | change source]
Mental health refers to a person's emotional and psychological well-being. "A state of emotional and psychological well-being in which an individual is able to use his or her thinking and emotional (feeling) abilities, function in society, and meet the ordinary demands of everyday life."

One way to think about mental health is by looking at how well a person functions. Feeling capable and efficient; being able to handle normal levels of stress, have good friends and family, and lead an independent life; and being able to "bounce back," or recover from hardships, are all signs of mental health.

Public health[change | change source]
Main page: Public health
Public health refers to trying to stop a disease that is unhealthy to the community, and does not help in long life or promote your health. This is fixed by organized efforts and choices of society, public and private clubs, communities and individuals.

It is about the health of many people, or everybody, rather than one person. Public health stops instead of encouraging a disease through surveillance of cases. To prevent being sick, it is good to do healthy behaviors such as hand washing, vaccination programs and using condoms. When infectious diseases break out, washing hands may be especially important.











Generally, the context in which an individual lives is of great importance for both his health status and quality of their life. It is increasingly recognized that health is maintained and improved not only through the advancement and application of health science, but also through the efforts and intelligent lifestyle choices of the individual and society. According to the World Health Organization, the main determinants of health include the social and economic environment, the physical environment, and the person's individual characteristics and behaviors.[9]

More specifically, key factors that have been found to influence whether people are healthy or unhealthy include the following:[9][10][11]

Income and social status
Social support networks
Education and literacy
Employment/working conditions
Social environments
Physical environments
Personal health practices and coping skills
Healthy child development
Biology and genetics
Health care services
Gender
Culture

Social support networks
Education and literacy
Employment/working conditions
Social environments
Physical environments
Personal health practices and coping skills
Healthy child development
Biology and genetics
Health care services
Gender












An increasing number of studies and reports from different organizations and contexts examine the linkages between health and different factors, including lifestyles, environments, health care organization, and health policy � such as the 1974 Lalonde report from Canada;[11] the Alameda County Study in California;[12] and the series of World Health Reports of the World Health Organization, which focuses on global health issues including access to health care and improving public health outcomes, especially in developing countries.[13]

The concept of the "health field," as distinct from medical care, emerged from the Lalonde report from Canada. The report identified three interdependent fields as key determinants of an individual's health. These are:[11]

Lifestyle: the aggregation of personal decisions (i.e., over which the individual has control) that can be said to contribute to, or cause, illness or death;
Environmental: all matters related to health external to the human body and over which the individual has little or no control;
Biomedical: all aspects of health, physical and mental, developed within the human body as influenced by genetic make-up.
The maintenance and promotion of health is achieved through different combination of physical, mental, and social well-being, together sometimes referred to as the "health triangle."[14][15] The WHO's 1986 Ottawa Charter for Health Promotion further stated that health is not just a state, but also "a resource for everyday life, not the objective of living. Health is a positive concept emphasizing social and personal resources, as well as physical capacities."[16]

Focusing more on lifestyle issues and their relationships with functional health, data from the Alameda County Study suggested that people can improve their health via exercise, enough sleep, maintaining a healthy body weight, limiting alcohol use, and avoiding smoking.[17] The ability to adapt and to self manage have been suggested as core components of human health.[18]

The environment is often cited as an important factor influencing the health status of individuals. This includes characteristics of the natural environment, the built environment, and the social environment. Factors such as clean water and air, adequate housing, and safe communities and roads all have been found to contribute to good health, especially to the health of infants and children.[9][19] Some studies have shown that a lack of neighborhood recreational spaces including natural environment leads to lower levels of personal satisfaction and higher levels of obesity, linked to lower overall health and well being.[20] This suggests that the positive health benefits of natural space in urban neighborhoods should be taken into account in public policy and land use.

Genetics, or inherited traits from parents, also play a role in determining the health status of individuals and populations. This can encompass both the predisposition to certain diseases and health conditions, as well as the habits and behaviors individuals develop through the lifestyle of their families. For example, genetics may play a role in the manner in which people cope with stress, either mental, emotional or physical. For example, obesity is a very large problem in the United States[citation needed] that contributes to bad mental health and causes stress in a lot of people's lives. (One difficulty is the issue raised by the debate over the relative strengths of genetics and other factors; interactions between genetics and environment may be of particular importance.)